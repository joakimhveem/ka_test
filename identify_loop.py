# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
# ms-python.python added
import os
try:
	os.chdir(os.path.join(os.getcwd(), '..\\..\..\..\..\AppData\Local\Temp'))
	print(os.getcwd())
except:
	pass
# %% [markdown]
# # Regelbaserad identifikation av kapitel i text
# 
# #### Principskiss över tillvägagångssätt att extrahera en kapitel från en text 
# 
# 1. Skär ut innehållsförteckningen 
# 
# 2. Använd innehållsförteckningen för att identifiera exakt namn på målkapitel och efterföljande kapitel. 
# 
# 3. Sök sedan i resten av dokumentet för att hitta radnumret för start och stopp. Extrahera innehållet och lägg in i en DataFrame (Pandas) 
# 
#  
# #### Detaljerade instruktioner 
# 
# 1. Skär ut innehållsförteckningen 
# Detta kan göras på flera sätt men borde inte vara så svårt. Typ sök efter första ”Innehåll*” till och skär av efter typ två sidor.  
# 
# 2. Identifiera nummer och namn på målkapitel och efterföljande kapitel  
#     a. Sök efter målkapitlet med en regexformel och lägg in i en lista med två element. 
#     b. Identifiera kapitelnamnet efteråt. Typ genom samma regexformel som för målkapitlet men nu med vad som helst som kapitelnamn. Här kan vi komplettera med if-satser om det skulle behövas. Exempelvis om vi inte hittar något kapitelnummer under målkapitlet så får vi söka efter ”Bilaga” eller ”Appendix”. Och om inget sådant hittas. Lägger vi in ordet ”_Slutkapitel”. 
#     c.Om vi inte hittar vårt målkapitel lägger vi in orden ”_inget_målkapitel” på båda namnplatserna och printar namnet på dokumentet i en ny lista som vi får gå igenom manuellt. Antingen måste regex-kommandot kalibreras eller så finns det inget sådant kapitel. 
# 
# 3. Hitta radstart och radslut för målkapitel (om det finns) och skär ut. Spara i en DataFrame. 
#     a. Sök nu igenom resten av dokumentet med kapitelnamnet. Om namnet finns med flera gånger så ska detta flaggas på något sätt. Men valet ska falla på den sista observationen. Samma gäller för efterföljande kapitel som markerar radslutet. 
#     b. Spara allt i en DataFrame 

# %%
# Importera paket
import os
from itertools import islice
import numpy as np
import pandas as pd
import codecs
import re

#pip install wordcloud
#from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
from PIL import Image

#nltk
import nltk
#nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import sent_tokenize, word_tokenize, RegexpTokenizer


# %%
os.getcwd()


# %%
path="C:\\Users\\joaki\\OneDrive\\Documents\\Datalab\\Notebooks\\KA_joakim\\Urval_1\\"


# %%
# Gör en lista av alla filer i min mapp
list_files=os.listdir(path)
# Kolla de första 10 filerna
print(list_files[0:9])
df_files=pd.DataFrame(list_files, columns=['namn'])
#pd.concat([df_files,pd.DataFrame(columns=['text','tokens','undantag','slutkapitel','relativ_pos'])], sort=False)
#df_files['txt']=df_files.copy()['namn']
df_files['text']=""
df_files['tokens']=np.nan
df_files['undantag']=""
df_files['slutkapitel']=""
df_files['relativ_pos']=np.nan # df.set_value(i,c,val) skapar vektorn och fyller i
#df_files=df_files.set_index('namn')
print(df_files.head())
counter=0



# %%
# En funktion för att tvätta matchade kapitel i innehållsförteckningen
def clean2(kap):
    kap=re.sub("\n+"," ",kap)
    kap=kap.replace("(","\(")
    kap=kap.replace(")","\)")
    kap=kap.replace("-","\-")
    kap=kap.strip()
    kap=kap.replace(" ","\s*") # Splittar på white space
    return kap

# %%
# En funktion för att tvätta matchade kapitel i innehållsförteckningen
def clean(kap):
    kap=re.sub("\n+"," ",kap)
    kap=re.sub("\.\.+"," ",kap) # Tar bort tecken som är 2 eller fler punkter på rad
    kap=kap.replace("(","\(")
    kap=kap.replace(")","\)")
    kap=kap.replace("-","\-")
    kap=kap.strip()
    kap=kap.replace(" ","\s*") # Splittar på white space
    return kap

# %% [markdown]
# #### Kör allt härifrån när du väl är igång

# %%
for lpnr in range(0,109): #range(0,100)
    print(df_files.at[lpnr,'namn'])

    # Använd index för att extrahera
    with open(path+str(df_files.at[lpnr,'namn']), encoding="utf-8", mode='r') as file:
        text=file.read()

    # 1.
    #########################################################################################################
    ################################ Hitta målkapitel #######################################################
    #########################################################################################################

    # r.M = MULTILINE söker över fler rader
    # r.I =IGNORECASE
    # ? = not gready. Behövs för att stanna innan punkterna

    #m=re.search('^[ \t]*(\d|1\d)\s+([Kk]onsekvens.*)',text, re.M)
    # Senast fungerande
    _regex='^[ \t]*(\d|1\d)\s+(\s*([åäö\w]+[ \t]+){0,4}konsekvens[åäö\w]+.*?)(?=\.*)'
    m=re.search(_regex,text, re.M + re.I)
    s_flag=-1
     # Fixa alla undantag...
    if m==None:
        #_regex='(\d+\s.*Statsfinansie.*\s*|.*offentligfinans.*|.*(\.\.+).*konsekvens.*|.*konsekvens.*(\.\.+).*)'
        _regex='(\d+\s*Statsfinansie.*\s*effekt\w*|.*offentligfinans.*|.*(\.\.+).*konsekvens.*|.*konsekvens.*(\.\.+).*)'
        m=re.search(_regex,text, re.I)
        if m==None:
            print("Hittar inget målkapitel... Tar nästa dokument i loop")
            df_files.at[lpnr,'text']="### Hittar inget målkapitel"
            continue
        else:
            print("--------------------------\nSpeciallösning... kontrollera")
            flag=lpnr
            df_files.at[lpnr,'undantag']="### Speciallösning, kontrollera"
            span_cont=m.span()
            _regex='(\d*)(.*)'
            mx=re.search(_regex,m.group(0), re.I)
            mg1=mx.group(1) # Siffran
            mg2=mx.group(2) # Titeln
            mg0=clean(mx.group(0))
            m2_text=mg0
    else:
        mg0=m.group(0)
        mg1=m.group(1)
        mg2=m.group(2)
        df_files.at[lpnr,'relativ_pos']=m.span()[0]/len(text)
        relativ_pos=m.span()[0]/len(text)
        if relativ_pos>0.3:
            print("regex returnerar inte innehållsförteckningen utan istället det faktiska avsnittet (troligen). Försök med ny regex")
            m2=clean(mg0)
            m2_list=m2
            _regex='(\d.*Statsfinansie.*\s*|.*offentligfinans.*|\d+\s*.*konsekvens.*)'
            m=re.search(_regex,text, re.I)
            relativ_pos=m.span()[0]/len(text)
            print(m)
            if relativ_pos < 0.3:
                print("Träff tidigare än förrut. Det kanske fungerade!")
                span_cont=m.span()
                print(m)
                df_files.at[lpnr,'undantag']=m
                print("--------------------------\nSpeciallösning... kontrollera")
                kap_clean=clean(m.group(0))
                m2=kap_clean
                m2_list=re.findall(kap_clean,text, re.M)
                m2_text=m2_list[-1]
            else:
                print(f"relativ position är fortfarande lika stor eller större än förrut: {round(relativ_pos,3)}. Notera och gå vidare.")
                df_files.at[lpnr,'Undantag']="relativ position är lite off"
                continue
        else:
            span_cont=m.span()
            print(m)
            print(m.group(0))
            print(mg2)
            print(clean(mg2))
            kap_clean=clean(mg2)
            m2=mg1+'[\s|\n\n]+'+kap_clean
            m2_list=re.findall(m2,text, re.M)
            m2_list[-1]
            m2_text=m2_list[-1]

    #Tar ut startpositionen och slutposition för det matchade uttrycket i en lista
    displace=span_cont[1]+2000 # Kan göras lite smartare. Typ söka från Kapitel 1 och nedåt.
    print(re.search(m2_text,text[displace:],  re.M + re.I))

    if re.search(m2_text,text[displace:],  re.M + re.I)==None:
        span=re.search(clean(m2_text),text[displace:],  re.M + re.I).span() # Sök från och med efter innehållsförteckningen
    else:     
        span=re.search(m2_text,text[displace:],  re.M + re.I).span() # Sök från och med efter innehållsförteckningen
    print(span)
    c_1=span[0]+displace
    c_2=span[1]+displace
    print(text[c_1:c_2])

    # Testa om det ser bra ut
    print(text[c_1:c_1+200])
    start_kap=c_1

    # 2.
    #########################################################################################################
    ########################### Hitta efterföljande kapitel #################################################
    #########################################################################################################

    # Kolla hur det ser ut
    text[span_cont[1]:span_cont[1]+300]

    # Hantera underrubriker. Börja här

    #_regex_s='(\s+\d*\s+\n+)(\d+)([\såäö\w]+.*?)(?=\.*)'
    #_regex_s='(\s+\d*\s+\n+)(\d+)?\.?([\såäö\w]+.*?)(?=\.\.*)' # Funkar för underkapitel
    _regex_s='(\s+\d*\s+\n+)(\d+)?\.?([\såäö\w]+.*?)(?=\.\.*)'
    ms=re.search(_regex_s,text[span_cont[1]:], re.M + re.I)
    # Handskas med irriterande sidhuvud "Prop."
    if ms.group(3)=='Prop':
        print("Träff på sidhuvud, sök senare")
        _regex_s='(\s+\d*\s+\n+)(\d+)?\.?([\såäö\w]+.*?)(?=\.\.*)'
        ms=re.search(_regex_s,text[span_cont[1]+10:], re.M + re.I)

    print(ms)
    print("-----------")
    print(ms.group(2))
    print("-----------")
    print(ms.group(3))

    print(repr(ms.group(1)))
    print(repr(ms.group(3)))
    print(repr(ms.group(2)))
    print("--------------------")

    # Avgör om efterföljande rad är en bilaga
    if re.match('^Bilaga',ms.group(3)):
        print("Efterföljande kapitel är en bilaga ")
        kk=int(mg1)+1
        s_flag=lpnr
        df_files.at[lpnr,'slutkapitel']="Bilaga"
    elif re.match('^Utdrag ur protokoll',ms.group(3)):
        print("Efterföljande kapitel är ett protokollutdrag ")
        kk=int(mg1)+1
        s_flag=lpnr
        df_files.at[lpnr,'slutkapitel']="Protokollutdrag"
    elif ms.group(2)!=None:
        if re.match('\d+',ms.group(2)):
            print("Grupp 2 är en siffra")
            kk=int(ms.group(2))
    elif ms.group(3)==None: 
        print("Grupp 3 finns inte") 
        kk=int(ms.group(2))
    else: 
        print("Grupp 3 finns")
        kk=int(ms.group(3))
    print(kk)   

    #Avgör om efterföljande rad är ett underkapitel
    if int(mg1)==kk:
        print(f"Kapitel {kk} har underkapitel")
        kap_nr='('+str(kk+1)+')'
        _regex_s='(\s+\d*\s+\n+)'+kap_nr+'(\s*\w*\s*\w*)(?=\.*)'
        ms=re.search(_regex_s,text[span_cont[1]:], re.M + re.I)
        if ms==None:
            print("Det finns inget kapitel efter sista underkapitlet. Troligen finns här en bilaga eller ett protokollutdrag")
            _regex_b='(^Bilaga\s\d)\s*([^.]+)'
            mb=re.search(_regex_b,text[span_cont[1]:], re.I + re.M)
            print(mb.group(1))
            if re.match('^Bilaga',mb.group(1)):
                print("Efterföljande kapitel är en bilaga ")
                kk=int(mg1)+1
                s_flag=lpnr
                df_files.at[lpnr,'slutkapitel']="Bilaga efter underrubriker"
                next_kap=mb.group(2)
            else:
                print("Efterföljande kapitel är inte en bilaga. KANSKE ett protokollutdrag. KONTROLLERA")
                #kk=int(mg1)+1
                #s_flag=lpnr
                df_files.at[lpnr,'undantag']="Något, inte en bilaga, efter underrubriker"
                next_kap=mb.group(0)
        else:
            print(ms)
            print(ms.group(2))
            print("---------")
            print(ms.group(3))  
            next_kap=ms.group(3)

    else:
        print(f"Kapitel {kk} har inga subkapitel som behöver hanteras")
        kap_nr=kk
        next_kap=ms.group(3)

    if ms==None:
        print("ms finns ej. Troligen en bilaga av nått slag")
        continue
   

    #Använd funktionen och rensa. Kolla att det blir rätt
    next_kap=clean(next_kap)
    print(repr(next_kap))

    if s_flag==lpnr:
        print("Efterföljande kapitel är en Bilaga eller ett protokollutdrag. Ingen behandling av kapitelnummer behövs.\n")
        ms2=next_kap
    else: 
        ms2=ms.group(2)+'\s*'+next_kap

    print(ms2)
    ms2_list=re.findall(ms2,text, re.M)
    displace=span_cont[1]+1000
    ms2_s=re.search(ms2,text[displace:], re.M)
    print(ms2_list)
    print(ms2_s)

    # Tar bort "Bilaga #" och använder bara rubriken till Bilaga #
    if ms2_s==None:
        _regex_b='(^Bilaga\s\d)\s*([^.]+)'
        mb=re.search(_regex_b,text[span_cont[1]:], re.I + re.M)
        print(mb)
        x=mb.group(2)
        print(x)
        x=re.sub("[\s]Prop","",x)
        ms2_sx=re.search(clean(x),text[displace:], re.M)
        print(ms2_sx)
        ms2_s=ms2_sx
        ms2_list=re.findall(ms2,text, re.M)
        print(ms2_list)
        counter=counter+1

    #Kontrollera att det är rätt ms2_s.start():ms2_s.end()+500
    correct=ms2_s.start()+displace
    print(text[correct:correct+1000])
    stop=correct

    # Klipp ut kapitlet
    kap=text[start_kap:stop]
    #print(kap)

    ##################################################
    #df_files.at[lpnr,'text']=kap
    df_files.at[lpnr,'text']=kap
    #df_files.at[lpnr,('tokens']=len(word_tokenize(kap))
    df_files.at[lpnr,'tokens']=len(word_tokenize(kap))
    my_dir=['m2_list','m2','flag','kap','kap_clean','kap_nr','kk','m','mb','mg0','mg1','mg2','ms','ms2','ms2_list','m2_text','ms2_s','ms2_sx','next_kap','relativ_pos','s_flag','span','span_cont','start_kap','stop','x']
    for varis in dir():
        if varis in my_dir and varis != "my_dir":
            del locals()[varis]
            print("deleting temporary variable: " +varis)
    # End loop

###############################################################    

df_files.head(20)


# %%
#df_files.head(30)
#print(df_files.iloc[14,3])
print(lpnr)
print(df_files.tokens.min())
print(df_files.tokens.count())
print(f"Andel funna kapitel {round((df_files.tokens.count()/len(df_files)),2)}")

# %%
print(df_files[df_files.tokens.isna()])


# %%
df_files[df_files.relativ_pos.notnull()].plot(y='relativ_pos', kind='bar')
# När den relativa positionen överstiger typ 30% har vi inte matchat på content...

# %%
df_files.sort_values('tokens',ascending=False).plot.bar(y='tokens')



# %%
