﻿Regeringens proposition 2011/12:153

Överklagande av Tullverkets vitesförelägganden

Prop.

2011/12:153
Regeringen överlämnar denna proposition till riksdagen.
Stockholm den 31 maj 2012
Carl Bildt
Peter Norman
(Finansdepartementet)
Propositionens huvudsakliga innehåll
I propositionen föreslås att Tullverkets vitesförelägganden enligt lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen ska få överklagas. Det föreslås också att sådana förelägganden ska gälla omedelbart.
Lagändringarna föreslås träda i kraft den 1 januari 2013.
1

Innehållsförteckning

Prop. 2011/12:153

1

Förslag till riksdagsbeslut.................................................................

3

2

Förslag till lag om ändring i lagen (1996:701) om Tullverkets

befogenheter vid Sveriges gräns mot ett annat land inom

Europeiska unionen ..........................................................................

4

3

Ärendet och dess beredning..............................................................

5

4

Bakgrund ..........................................................................................

5

5

Vitesförelägganden ska få överklagas ..............................................

6

6

Konsekvensanalys ............................................................................

7

7

Författningskommentar.....................................................................

8

Bilaga 1

Sammanfattning av promemorian Överklagande av

Tullverkets vitesförelägganden...............................................

9

Bilaga 2

Promemorians lagförslag......................................................

10

Bilaga 3

Förteckning över remissinstanserna .....................................

11

Bilaga 4

Lagrådets yttrande ................................................................

12

Utdrag ur protokoll vid regeringssammanträde den 31 maj 2012...........

13
2

1

Förslag till riksdagsbeslut

Prop. 2011/12:153
Regeringen föreslår att riksdagen antar regeringens förslag till lag om ändring i lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen.
3

2

Förslag till lag om ändring i lagen (1996:701) 
Prop. 2011/12:153

om Tullverkets befogenheter vid Sveriges

gräns mot ett annat land inom Europeiska

unionen
Härigenom föreskrivs att 21 och 22 §§ lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom

Europeiska unionen ska ha följande lydelse.

Nuvarande lydelse

Föreslagen lydelse

21 §
1

Vite får av 
Tullverket 
föreläggas

Tullverket 
får förelägga

1. ett

transportföretag 
som inte

1. ett transportföretag 
att full-

fullgör

sina skyldigheter enligt

göra 
sina skyldigheter enligt 15 §,

15 §,

och

2. den vars uppgifter 
skall 
kon-

2. den

vars

uppgifter

ska

trolleras eller för vars räkning en

kontrolleras eller för vars räkning

vara införs eller utförs enligt 13 §

en vara införs eller utförs enligt

och som inte fullgör 
sina skyldig-

13 § 
att fullgöra 
sina skyldigheter

heter enligt samma paragraf.

enligt samma paragraf.

Ett

beslut

om

föreläggande

enligt första stycket får förenas

med vite. Sådana beslut gäller

omedelbart.

22 §
2

Beslut enligt 
1315
 och

17

Beslut

enligt

1315

och

17

17 d §§ får överklagas 
hos 
allmän

17 d §§

samt 21 § andra stycket

förvaltningsdomstol.

får

överklagas

till

allmän

för

förvaltningsdomstol.

vid

Prövningstillstånd krävs

Prövningstillstånd

krävs

överklagande till kammarrätten.

överklagande till kammarrätten.

Övriga 
beslut enligt denna lag

Andra 
beslut enligt denna lag får

får inte överklagas.

inte överklagas.

Denna lag träder i kraft den 1 januari 2013 och tillämpas på vitesförelägganden som meddelas efter utgången av 2012.

1

Senaste lydelse 1999:434.

2

Senaste lydelse 1999:434.

4

3

Ärendet och dess beredning

Prop. 2011/12:153
Inom Finansdepartementet har upprättats en promemoria innehållande förslag om att vitesförelägganden enligt lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen ska få överklagas och att sådana beslut ska gälla omedelbart. En sammanfattning av promemorian finns i 
bilaga 1
. Promemorians lagförslag finns i 
bilaga 2
. Promemorian har remissbehandlats. En förteckning över remissinstanserna finns i 
bilaga 3
. En sammanställning av remissyttrandena finns tillgänglig i Finansdepartementet (dnr Fi2012/1151).
I propositionen behandlas promemorians förslag.
Lagrådet
Regeringen beslutade den 10 maj 2012 att inhämta Lagrådets yttrande över ett lagförslag som är likalydande med lagförslaget i propositionen. Lagrådet lämnade förslaget utan erinran. Lagrådets yttrande finns i 
bilaga 4
.
4 Bakgrund
Tullverkets befogenheter vid införsel eller utförsel över Sveriges gräns mot ett annat 
EU-land
 regleras i lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen, benämnd inregränslagen. Tullverkets befogenheter i den reguljära kontrollverksamheten i förhållande till tredje land regleras främst i tullagen (2000:1281).
Av 13 § inregränslagen framgår att vid kontroll ska den vars uppgifter ska kontrolleras, eller för vars räkning en vara införs eller utförs, ge den som verkställer kontrollen tillfälle att bl.a. undersöka varan och transportmedlet. Han eller hon ska också svara för bl.a. uppackning och återinpackning av den undersökta varan. Tullverket får enligt 21 § inregränslagen vitesförelägga den som inte fullgör sina skyldigheter enligt 13 §.
Enligt 15 § inregränslagen ska transportföretag på begäran av Tullverket skyndsamt lämna aktuella uppgifter om ankommande och avgående transporter som företaget har tillgång till. Om transportföretaget inte fullgör dessa skyldigheter får Tullverket enligt 21 § inregränslagen förelägga företaget vite.
I 22 § första stycket inregränslagen finns en uppräkning av vilka beslut enligt den lagen som får överklagas till allmän förvaltningsdomstol. Bland de beslut som räknas upp återfinns bl.a. Tullverkets beslut enligt 13 och 15 §§. Tullverkets beslut enligt 21 § om föreläggande vid vite finns emellertid inte med i uppräkningen och får därmed enligt 22 § tredje stycket inregränslagen inte överklagas.
5
Tullagen innehåller, till skillnad från inregränslagen, inte något Prop. 2011/12:153 överklagandeförbud avseende Tullverkets beslut om föreläggande som
har förenats med vite, jfr 9 kap. 2 § tullagen.
Inom det förvaltningsrättsliga området får beslut om förelägganden som har förenats med vite normalt sett överklagas. Efter att skatteförfarandelagen (2011:1244) trädde i kraft den 1 januari 2012 gäller detta även inom skatteförfarandeområdet (se prop. 2010/11:165 s. 613 f.). Från och med den 1 juli 2012 kommer också vitesförelägganden inom fastighetstaxeringsområdet att få överklagas (prop. 2011/12:96, bet. 2011/12:SkU19, rskr. 2011/12:208, SFS 2012:271).
5 Vitesförelägganden ska få överklagas

Regeringens förslag: 
Beslut enligt inregränslagen om föreläggande som har förenats med vite ska få överklagas. Sådana beslut ska gälla omedelbart.

Promemorians förslag: 
Överensstämmer med regeringens förslag. 
Remissinstanserna: 
Samtliga remissinstanser som har yttrat sig
tillstyrker eller har inget att erinra mot förslaget.

Skälen för regeringens förslag: 
Tullverkets beslut enligt inregränslagen om föreläggande som har förenats med vite får inte överklagas. Det skiljer sig från vad som gäller enligt tullagen och även inom skatteförfarandeområdet där vitesförelägganden numera får överklagas.
Överklagandeförbudet uppvägs i viss mån av att den som är missnöjd med ett vitesföreläggande kan framföra sina invändningar i förvaltningsrättens mål om utdömande av vitet.
Det enda sättet för den som har förelagts vite enligt inregränslagen att få vitesföreläggandet prövat av domstol är således att inte följa det och därmed riskera att vitet döms ut om domstolen inte godtar invändningarna.
Det har ansetts vara en klar fördel om en förelagd som har invändningar mot ett vitesföreläggande kan få föreläggandet prövat av domstol utan att för den skull behöva riskera att vitet döms ut (prop. 2010/11:165 s. 614). Utgångspunkten bör därför vara att vitesförelägganden enligt inregränslagen ska få överklagas.
Frågan blir då om ett slopande av överklagandeförbudet i inregränslagen skulle medföra så stora nackdelar att förbudet bör behållas. Det är främst effektivitetsskäl som kan motivera ett överklagandeförbud. Grunden för det argumentet är att en rätt att överklaga vitesföreläggandet skulle ge den förelagde en möjlighet att förhala fullgörandet av sina skyldigheter (jfr prop. 2010/11:165 s. 614).
Av stor betydelse för ett vitesföreläggandes effektivitet är när beslutet vinner laga kraft. Ett vitesföreläggande som får överklagas vinner, utan uttryckliga bestämmelser om motsatsen, inte laga kraft förrän tiden för överklagande har gått ut. Den förelagde kan genom att överklaga föreläggandet skjuta fram tidpunkten då föreläggandet vinner laga kraft
6
och därmed också sin skyldighet att göra det som han eller hon har Prop. 2011/12:153 förelagts.
Ett sätt att begränsa den förelagdes möjligheter att utan befogade skäl förhala processen är att låta beslut om föreläggande som har förenats med vite gälla omedelbart. Eftersom besluten gäller omedelbart, stoppas bara processen om den domstol som ska pröva ett överklagande beslutar att beslutet tills vidare inte ska gälla, s.k. inhibition. Ogrundade överklaganden lär inte leda till inhibition och att överklaga enbart i syfte att förhala går då inte. Med en ordning där beslut om föreläggande som har förenats med vite gäller omedelbart bör effektivitetsskäl således inte kunna åberopas som stöd för att förbjuda överklaganden (jfr prop. 2010/11:165 s. 614 f.).
Mot denna bakgrund och då ingen av remissinstanserna har haft några invändningar framstår det enligt regeringens mening som klart att vitesförelägganden enligt inregränslagen ska kunna överklagas och att sådana beslut ska gälla omedelbart. Överklagandeförbudet ska därmed slopas.
Förslaget föranleder ändringar i 21 och 22 §§ inregränslagen.
6 Konsekvensanalys

Regeringens bedömning: 
Förslaget får inte några offentligfinansiella konsekvenser. Förslaget ökar inte Tullverkets och de allmänna förvaltningsdomstolarnas arbetsbörda i någon nämnvärd utsträckning.
Förslaget är från rättssäkerhetssynpunkt positivt för företag och andra enskilda.

Promemorians bedömning: 
Överensstämmer med regeringens bedömning.

Remissinstanserna: 
Kammarrätten i Stockholm 
anser att frågan om huruvida eventuella merkostnader för de allmänna förvaltningsdomstolarna ska rymmas inom domstolarnas befintliga anslagsramar inte är tillräckligt utredd. Kammarrätten påtalar även att förslaget om att vitesförelägganden ska gälla omedelbart kan ge en ökning av antalet mål med inhibitionsyrkanden, vilket kan bli betungande för domstolarna. 
Domstolsverket 
påpekar att ett flertal mindre resurskrävande reformer, som var för sig medför visst antal ytterligare mål i domstol, sammantagna kan medföra att ett resurstillskott till domstolarna är nödvändigt.

Skälen för regeringens bedömning: 
Enligt uppgift från Tullverket är de vitesförelägganden som meddelas med stöd av 21 § 2 inregränslagen extremt få och de är aldrig fler än en handfull per år. När det gäller vitesförelägganden enligt 21 § 1 inregränslagen gentemot transportföretag att lämna uppgifter, har Tullverket hittills aldrig utnyttjat den möjligheten.
Antalet beslut om förelägganden som överklagas till domstol torde således bli ytterst litet. Regeringen finner mot denna bakgrund att förslaget inte får några offentligfinansiella konsekvenser. Inte heller
7
bedöms förslaget öka Tullverkets arbetsbörda i någon nämnvärd Prop. 2011/12:153 utsträckning.
I fråga om arbetsbördan för de allmänna förvaltningsdomstolarna noterar regeringen att 
Förvaltningsrätten i Stockholm 
har lämnat förslagen utan erinran och att 
Statskontoret 
inte har haft några synpunkter att redovisa. Regeringen anser, även med beaktande av vad
Kammarrätten i Stockholm 
och 
Domstolsverket 
har påtalat, att eventuella merkostnader för de allmänna förvaltningsdomstolarna ska rymmas inom domstolarnas befintliga anslagsramar.

För företagen och andra enskilda är det från rättssäkerhetssynpunkt positivt att få möjlighet till domstolsprövning av ett vitesföreläggande redan när själva föreläggandet beslutas.
7 Författningskommentar
Förslaget till lag om ändring i lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen
21 §
I 
första stycket 
ges Tullverket befogenhet att förelägga enskilda att fullgöra de skyldigheter som anges i 13 och 15 §§. Bemyndigandet för Tullverket att förena ett beslut om föreläggande med vite flyttas till det nya 
andra stycket
. Härigenom förtydligas det som i praktiken gäller redan i dag, nämligen att Tullverket får meddela osanktionerade förelägganden med stöd av förevarande paragraf. I andra stycket anges även att beslut om föreläggande som har förenats med vite gäller omedelbart. Ett sådant beslut kan således verkställas utan hinder av att det överklagats såvida inte domstolen beslutar om s.k. inhibition.
22 §
Ändringen i 
första stycket 
innebär att Tullverkets beslut enligt 21 § andra stycket, dvs. beslut om föreläggande som har förenats med vite, får överklagas till allmän förvaltningsdomstol. Beslut enligt 21 § första stycket om föreläggande som inte har förenats med vite räknas inte upp och får därför enligt tredje stycket inte överklagas.
Det har även gjorts några språkliga justeringar i paragrafen.
Ikraftträdande
Lagen träder i kraft den 1 januari 2013 och tillämpas på vitesförelägganden som meddelas efter utgången av 2012.
8
Sammanfattning av promemorian Överklagande av Tullverkets vitesförelägganden
I promemorian föreslås att Tullverkets vitesförelägganden enligt lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen ska få överklagas. Det föreslås också att beslut om föreläggande enligt den lagen som har förenats med vite ska gälla omedelbart.
Bestämmelserna föreslås träda i kraft den 1 januari 2013.
Prop. 2011/12:153
Bilaga 1
9
Promemorians lagförslag
Prop. 2011/12:153
Bilaga 2
Förslag till lag om ändring i lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen
Härigenom föreskrivs att 21 och 22 §§ lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen ska ha följande lydelse.

Nuvarande lydelse

Föreslagen lydelse
Vite får av 
Tullverket 
föreläggas
1. ett transportföretag 
som inte fullgör 
sina skyldigheter enligt

15

§,

2.

den vars uppgifter 

skall 
kontrolleras eller för vars räkning en vara införs eller utförs enligt

13

§ 
och som inte fullgör 
sina skyldigheter enligt samma paragraf.

21 §
1

Tullverket 
får vid vite förelägga

1.

ett transportföretag 

att fullgöra 
sina skyldigheter enligt 15 §, 
och

2.

den vars uppgifter 

ska 
kontrolleras eller för vars räkning en vara införs eller utförs enligt 13 § 
att fullgöra 
sina skyldigheter enligt samma paragraf.
Beslut om föreläggande som har förenats med vite gäller omedelbart.
22 §
2

Beslut enligt 
1315
och

17

Beslut enligt

1315
,

17

17 d §§ får överklagas 
hos 
allmän

17 d 
och 21 
§§ får överklagas 
till

förvaltningsdomstol.

allmän förvaltningsdomstol.

Prövningstillstånd krävs

för

Prövningstillstånd

krävs

för

överklagande till 
kammarrätten
.

överklagande till 
kammarrätt
.

Övriga beslut enligt denna lag får inte överklagas.

Denna lag träder i kraft den 1 januari 2013 och tillämpas på vitesförelägganden som meddelas efter utgången av 2012.

1

Senaste lydelse 1999:434.

2

Senaste lydelse 1999:434.

10
Förteckning över remissinstanserna
Prop. 2011/12:153
Bilaga 3
Efter remiss har yttranden kommit in från Kammarrätten i Stockholm, Förvaltningsrätten i Stockholm, Justitiekanslern, Domstolsverket, Tullverket och Sveriges advokatsamfund.
Åklagarmyndigheten och Statskontoret har angett att de inte har några synpunkter i ärendet.
Regelrådet har avstått från att yttra sig.
Svensk Handel och Transportindustriförbundet har inte avgett några yttranden.
11
Lagrådets yttrande
Utdrag ur protokoll vid sammanträde 
2012-05-15

Närvarande: 
F.d. justitieråden Marianne Eliason och Peter Kindlund samt justitierådet Kerstin Calissendorff.
Överklagande av Tullverkets vitesförelägganden
Enligt en lagrådsremiss den 10 maj 2012 (Finansdepartementet) har regeringen beslutat att inhämta Lagrådets yttrande över förslag till
lag om ändring i lagen (1996:701) om Tullverkets befogenheter vid Sveriges gräns mot ett annat land inom Europeiska unionen.
Förslaget har inför Lagrådet föredragits av rättssakkunnige Torbjörn Almerheim, biträdd av departementssekreteraren Marcus Sjögren.

Lagrådet 
lämnar förslaget utan erinran.
Prop. 2011/12:153
Bilaga 4
12
Finansdepartementet
Utdrag ur protokoll vid regeringssammanträde den 31 maj 2012
Närvarande: statsråden Bildt, ordförande, Ask, Erlandsson, Hägglund, Sabuni, Billström, Ohlsson, Norman, Attefall, Engström, Kristersson, 
Elmsäter-Svärd,
 Ullenhag, Hatt, Ek, Enström
Föredragande: statsrådet Norman
Regeringen beslutar proposition 2011/12:153 Överklagande av Tullverkets vitesförelägganden.
Prop. 2011/12:153
13