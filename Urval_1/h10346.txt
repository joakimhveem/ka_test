﻿Regeringens proposition 2013/14:46

Regionalt utvecklingsansvar i Jönköpings,

Prop.

Örebro och Gävleborgs län

2013/14:46
Regeringen överlämnar denna proposition till riksdagen.
Stockholm den 21 november 2013
Fredrik Reinfeldt
Stefan Attefall
(Socialdepartementet)
Propositionens huvudsakliga innehåll
I propositionen föreslås ändring av lagen (2010:630) om regionalt utvecklingsansvar i vissa län. Lagförslaget medför att lagen även omfattar Jönköpings, Örebro och Gävleborgs län och gäller för landstingen i dessa län. Förslaget innebär vidare att Jönköpings läns landsting, Örebro läns landsting och Gävleborgs läns landsting får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelse. Vid val ska dock beteckningen landstingsfullmäktige användas.
Lagändringen föreslås träda i kraft den 1 januari 2015.
1

Prop. 2013/14:46

Innehållsförteckning

1

Förslag till riksdagsbeslut .................................................................

3

2

Förslag till lag om ändring i lagen (2010:630) om regionalt

utvecklingsansvar i vissa län.............................................................

4

3

Ärendet och dess beredning ..............................................................

6

4

Nuvarande ordning

............................................................................

6

4.1

Det regionala ......................................utvecklingsansvaret

6

4.1.1 .....................................

Regionalt tillväxtarbete

7

4.1.2

Länsplaner för regional

.........................................

transportinfrastruktur

8

5

Statliga uppgifter till .................................................vissa landsting

8

6

Konsekvenser..................................................................................

11

7

Ikraftträdande och övergångsbestämmelser ....................................

12

8

Författningskommentar...................................................................

13

Bilaga 1 Sammanfattning av departementspromemorian

Regionalt utvecklingsansvar i Jönköpings län (Ds

2012:55)................................................................................

14

Bilaga 2 Lagförslag i departementspromemorian Regionalt

utvecklingsansvar ................i Jönköpings län (Ds 2012:55)

15

Bilaga 3 Förteckning över remissinstanserna avseende

departementspromemorian Regionalt utvecklingsansvar

i Jönköpings ..............................................län (Ds 2012:55)

17

Bilaga 4 Sammanfattning av departementspromemorian

Regionalt utvecklingsansvar i Örebro län och

Gävleborgs län ................................................(Ds 2013:14)

18

Bilaga 5 Lagförslag i departementspromemorian Regionalt

utvecklingsansvar i Örebro län och Gävleborgs län (Ds

2013:14)................................................................................

19

Bilaga 6 Förteckning över remissinstanserna avseende

departementspromemorian Regionalt utvecklingsansvar

i Örebro län och .....................Gävleborgs län (Ds 2013:14)

21

Utdrag ur protokoll vid regeringssammanträde den 21 november

2013 .......................................................................................

22
2

1

Förslag till riksdagsbeslut

Prop. 2013/14:46
Regeringen föreslår att riksdagen antar regeringens förslag till lag om ändring i lagen (2010:630) om regionalt utvecklingsansvar i vissa län.
3

Prop. 2013/14:46 
2

Förslag till lag om ändring i lagen

(2010:630) om regionalt

utvecklingsansvar i vissa län
Härigenom föreskrivs att 1, 3 och 4 §§ lagen (2010:630) om regionalt utvecklingsansvar i vissa län ska ha följande lydelse.

Nuvarande lydelse

Föreslagen lydelse

1 §

I denna lag finns bestämmelser

I denna lag finns bestämmelser

om regionalt tillväxtarbete och om

om regionalt tillväxtarbete och om

länsplaner för regional transport-

länsplaner för regional transport-

infrastruktur i Gotlands, Skåne,

infrastruktur i 
Jönköpings, 
Got-

Hallands 
och 
Västra Götalands län.

lands,

Skåne, Hallands, Västra

Götalands, 
Örebro och Gävleborgs

län
.

3 §
1

Denna lag gäller för Skåne läns

Denna lag gäller för 
Jönköpings

landsting, Hallands läns landsting

läns landsting, 
Skåne läns lands-

och 
Västra Götalands läns lands-

ting,

Hallands

läns

landsting,

ting. Lagen gäller också, med

Västra

Götalands

läns

landsting,

undantag för 8 § 1, för Gotlands

Örebro läns landsting och Gävle-

kommun. Det som sägs om ett

borgs läns landsting
. Lagen gäller

landsting i 
5-10
 §§ gäller i sådant

också, med undantag för 8 § 1, för

fall kommunen.

Gotlands kommun. Det som sägs

om ett landsting i 
5-10
 §§ gäller i

sådant fall kommunen.

4 §
2

De landsting som avses i denna lag får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om landstingsfullmäktige och landstingsstyrelsen gäller i sådant fall regionfullmäktige och regionstyrelsen.
Gotlands kommun får besluta att kommunfullmäktige och kommunstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om kommunfullmäktige och kommunstyrelsen gäller i sådant fall regionfullmäktige och regionstyrelsen.
Vid genomförande av val enligt Vid genomförande av val enligt vallagen (2005:837) ska dock vallagen (2005:837) ska dock beteckningen landstingsfullmäktige beteckningen landstingsfullmäktige användas vid val i Skåne läns användas vid val i 
Jönköpings läns

1

Senaste lydelse 2011:1193.

2

Senaste lydelse 2011:1193.

4

landsting, Hallands

läns landsting

landsting,

Skåne läns landsting, Prop. 2013/14:46

och 
Västra Götalands läns lands-

Hallands läns landsting, Västra Gö-

ting. Vid val i Gotlands kommun

talands läns landsting, 
Örebro läns

ska beteckningen

kommunfull-

landsting

och Gävleborgs läns

mäktige användas.

landsting
. Vid val i Gotlands kom-

mun ska beteckningen kommun-

fullmäktige användas.
Denna lag träder i kraft den 1 januari 2015.
5
Prop. 2013/14:46
3 Ärendet och dess beredning
Inom Regeringskansliet har departementspromemoriorna Regionalt utvecklingsansvar i Jönköpings län (Ds 2012:55) och Regionalt utvecklingsansvar i Örebro län och Gävleborgs län (Ds 2013:14) utarbetats. Promemoriorna innehåller förslag till ändring av lagen (2010:630) om regionalt utvecklingsansvar i vissa län så att lagen också ska omfatta Jönköpings län, Örebro län och Gävleborgs län. Lagförslagen föreslås träda i kraft den 1 januari 2015.
Sammanfattningar av promemoriorna Ds 2012:55 och Ds 2013:14 finns i bilagorna 1 och 4. Promemoriornas lagförslag återges i bilagorna 2 och 5. Promemoriorna har remissbehandlats. Förteckningar över remissinstanserna finns i bilagorna 3 och 6. Sammanställningar av remissvaren finns tillgänglig i Socialdepartementet (dnr S2012/8662/SFÖ och dnr S2013/2055/SFÖ).
4 Nuvarande ordning

4.1

Det regionala utvecklingsansvaret

Det regionala utvecklingsansvaret omfattar vissa uppgifter i det regionala tillväxtarbetet och upprättande av samt fastställande av länsplaner för regional transportinfrastruktur. Utvecklingsansvaret är fördelat på olika sätt i länen. Enligt lagen om regionalt utvecklingsansvar har landstingen i Skåne, Hallands och Västra Götalands län sedan 2011 det regionala utvecklingsansvaret i respektive län. I Gotlands län har Gotlands kommun ansvaret enligt samma lag, eftersom kommunen är en landstingsfri kommun. I Uppsala, Södermanlands, Östergötlands, Jönköpings, Kronobergs, Kalmar, Blekinge, Värmlands, Örebro, Dalarnas, Gävleborgs, Jämtlands och Västerbottens län har samverkansorgan det regionala utvecklingsansvaret. Med samverkansorgan avses en kommunalt beslutande församling inom ett län med särskild uppgift att svara för regionala utvecklingsfrågor i länet. Sedan 2003 har kommuner och landsting i ett län möjlighet att bilda samverkansorgan enligt lagen (2002:34) om samverkansorgan i länen. Utöver de uppgifter som utvecklingsansvaret för med sig kan samverkansorganets medlemmar välja att ge organet kommunala uppgifter. I Stockholms, Västmanlands, Västernorrlands och Norrbottens län har länsstyrelsen det regionala utvecklingsansvaret.
Länsstyrelserna svarar i samtliga län enligt förordningen (2007:825) med länsstyrelseinstruktion för den statliga förvaltningen i länet, i den utsträckning inte någon annan myndighet har ansvaret för särskilda förvaltningsuppgifter (1 § länsstyrelseinstruktionen). Länsstyrelserna ska verka för att nationella mål får genomslag i länen samtidigt som hänsyn ska tas till regionala förhållanden och förutsättningar. Länsstyrelserna
ska utifrån ett statligt helhetsperspektiv arbeta sektorsövergripande och
6
inom myndigheternas ansvarsområden samordna olika samhällsintressen Prop. 2013/14:46 och statliga myndigheters insatser. Länsstyrelserna ska främja länets
utveckling och noga följa tillståndet i länet (2 §).
4.1.1 Regionalt tillväxtarbete

Regionalt tillväxtarbete avser insatser för att skapa hållbar regional

tillväxt och utveckling. Det regionala tillväxtarbetet syftar till att uppfylla

den regionala tillväxtpolitikens mål och främja en hållbar regional

tillväxt. Den regionala tillväxtpolitikens inriktning läggs fast i den

nationella strategin för regional konkurrenskraft, entreprenörskap och

sysselsättning 
2007-2013
 och i den årliga budgetpropositionen (utgifts-

område 19 Regional tillväxt). Styrning av det regionala tillväxtarbetet

sker även genom fördelning av anslag inom utgiftsområde 19 Regional

tillväxt, främst anslag 1:1 
Regionala tillväxtåtgärder 
och anslag 1:3

Europeiska regionala utvecklingsfonden perioden 
2007-2013
och anslag

1.4 
Europeiska regionala utvecklingsfonden perioden 
2014-2020.

Arbetet med regionala tillväxt- och utvecklingsfrågor regleras av bl.a.

lagen (2002:34) om samverkansorgan i länen, lagen (2010:630) om

regionalt utvecklingsansvar i vissa län, förordningen (2007:713) om

regionalt tillväxtarbete, förordningen (2003:596) om bidrag för

projektverksamhet inom den regionala tillväxtpolitiken samt för-

ordningen (1997:263) om länsplaner för regional transportinfrastruktur.

Därutöver finns ett antal förordningar om regionala företagsstöd. Enligt

förordningen (2008:81) om stöd till anläggning av kanalisation

ankommer vissa uppgifter länsstyrelserna eller den organisation som har

det regionala utvecklingsansvaret enligt lagen om regionalt utvecklings-

ansvar i vissa län.

Det regionala tillväxtarbetet omfattar ett flertal olika uppgifter. En

uppgift är att utarbeta och fastställa en strategi för länets utveckling och

samordna genomförandet av strategin. I utarbetandet och genomförandet

av strategin ska samråd och samverkan ske med kommuner och

landsting. Samråd och samverkan bör även ske med näringsliv och

organisationer. Det regionala tillväxtarbetet består vidare av att besluta

om användningen av vissa statliga medel för regionalt tillväxtarbete. En

ytterligare uppgift är att följa upp, låta utvärdera och till regeringen

årligen rapportera resultaten av det regionala tillväxtarbetet.

I samtliga län har länsstyrelserna enligt förordningen (2007:713) om

regionalt tillväxtarbete ett ansvar att följa upp hur andra statliga

myndigheter tillämpar det regionala utvecklingsprogrammet (18 § 1).

Länsstyrelsen ska vidare främja och samordna andra statliga myndig-

heters insatser för regional tillväxt i länet (18 § 2), och i förekommande

fall, löpande informera samverkansorganet eller berörda landsting samt

Gotlands kommun om andra statliga myndigheters insatser för regional

tillväxt i länet.

Det regionala tillväxtarbetet finansieras av flera aktörer och ofta genom

olika former av medfinansiering. Anslaget 1:1 
Regionala tillväxtåtgärder

inom utgiftsområde 19 används för projektverksamhet (inklusive

riskkapitalfonder, lånefonder och garantifonder), regionalt investe-

ringsstöd, regionalt bidrag till företagsutveckling, sysselsättningsbidrag,

7

Prop. 2013/14:46

såddfinansiering, ersättning för vissa kreditförluster i stödområde B samt

stöd till kommersiell service. Anslaget får också användas för viss

administration, uppföljning och utvärdering samt viss central utveck-

lingsverksamhet, m.m.

Enligt budgetpropositionen för 2013 bör det även fortsättningsvis vara

en uppgift för regeringen att fördela anslaget mellan län och

anslagsposter och utfärda de föreskrifter som behövs. Vid fördelning av

medel mellan länen utgör bl.a. behov av medfinansiering av EU:s

strukturfonder, stödområdestillhörighet samt särskilda omställnings-

problem de huvudsakliga fördelningsnycklarna, med särskilt beaktande

av medfinansieringsbehovet av åtgärder inom ramen för det regionala

tillväxtarbetet.
4.1.2 Länsplaner för regional transportinfrastruktur
Trafikverket ansvarar för att upprätta en nationell och trafikslagsövergripande plan för transportinfrastruktur till ledning för fördelning av statliga medel. Inom varje län ska det upprättas en länsplan till ledning vid fördelning av medel för investeringar och förbättringsåtgärder. Länsplanen upprättas och fastställs av de länsstyrelser, samverkansorgan och landsting samt Gotlands kommun som har det regionala utvecklingsansvaret.
I arbetet med planering av transportinfrastruktur har länsstyrelsen i samtliga län ett prövningsansvar kopplat till den fysiska planeringsprocessen. Till exempel ska länsstyrelserna godkänna upprättade miljökonsekvensbeskrivningar, avge yttranden över arbetsplaner för vägar och järnvägar samt pröva vissa tillstånd enligt miljöbalken.

5

Statliga uppgifter till vissa landsting

Regeringens bedömning: 
Jönköpings läns landsting, Örebro läns

landsting och Gävleborgs läns landsting bör ha det regionala

utvecklingsansvaret i länet.

Regeringens förslag: 
Lagen (2010:630) om regionalt utveck-

lingsansvar i vissa län ska omfatta Jönköpings, Örebro och

Gävleborgs län.

Promemoriornas bedömningar och förslag 
överensstämmer med

regeringens.

Remissinstanserna: 
Förslaget att Jönköpings läns landsting får det

regionala utvecklingsansvaret i länet tillstyrks i stort av 
Länsstyrelsen i

Östergötlands, Jönköpings, Kronobergs 
och 
Hallands län
. 
Länsstyrelsen

i Jönköpings län 
tillstyrker dock inte förslaget i den del som avser

förändringar i fördelningen av de statliga regionala tillväxtmedlen, via

anslag 1:1 
Regionala tillväxtåtgärder
, som samverkansorganet och

länsstyrelsen i dag hanterar. Länsstyrelsen anser att nuvarande fördelning

8

bör

behållas. 
Länsstyrelsen i Kronobergs län 
och 
Länsstyrelsen i

Östergötlands län 
anser att när det gäller länsstyrelsens uppdrag att besluta om företagsstöd och stöd till kommersiell service finns det en stark koppling till genomförandet av Landsbygdsprogrammet, vilket hanteras av länsstyrelsen. Vidare finns koppling till länsstyrelsens bevaknings- och utvecklingsuppdrag inom området grundläggande betaltjänster. De två länsstyrelserna påtalar fördelarna med att dessa landsbygdsrelaterade uppdrag även framgent hanteras samlat av länsstyrelsen. 
Länsstyrelsen i Hallands län 
framhåller att uppgiften att besluta om vissa statliga medel till regional projektverksamhet och regionala företagsstöd är en viktig del i det regionala tillväxtarbetet. Att samla besluten om dessa medel till ett organ ger ökad tydlighet. Samtidigt bör länsstyrelsen garanteras resurser för samordning av statliga intressen och för medverkan och finansiering i utvecklingsinsatser, vilket också ökar samarbetsförutsättningarna i länet. 
Trafikverket, Tillväxtverket, Statens kulturråd 
och 
Jönköpings läns landsting m.fl. 
tillstyrker förslaget i sin helhet. 
Regionförbundet i Jönköpings län 
ser med tillfredsställelse på förslaget att lagen om regionalt utvecklingsansvar i vissa län även ska omfatta Jönköpings län. 
Örebro läns landsting m.fl
. förutsätter att ansvar och resurser hänger samman och att finansieringsprincipen ska gälla för de uppgifter som förs över. 
Regionförbundet i Kalmar län 
och 
Regionförbundet i Södra Småland 
anser att det är viktigt att medel som länsstyrelsen i dag disponerar för företagsstöd och projektmedel överförs med samma fördelning som i övriga län där landstinget har det regionala utvecklingsansvaret. 
Jönköpings, Aneby, Gnosjö 
och 
Tranås m.fl. kommuner 
i länet tillstyrker förslaget. 
Sveriges kommuner och landsting (SKL) 
delar promemorians bedömning och tillstyrker förslaget. 
Företagarna 
anför att de i huvudsak är positiva till tanken med regionkommuner.
Kring promemorian Regionalt utvecklingsansvar i Örebro län och Gävleborgs läns förslag råder det delade meningar. 
Länsstyrelsen i Västmanlands län m.fl. länsstyrelser 
avstyrker förslaget att landstingen i de två länen tilldelas utvecklingsansvaret. Som skäl anges bl.a. att en överföring av ansvar enligt promemorians förslag inte bör genomföras innan en övergripande samlad bedömning har gjorts av ansvarsfördelningen av det regionala utvecklingsarbetet. 
Länsstyrelsen i Värmlands län m.fl. länsstyrelser 
för även fram vikten av kopplingen mellan företagsstöden och Landsbygdsprogrammet. 
Länsstyrelsen i Dalarnas län 
anser att förslaget kommer, om det genomförs, försvåra för länsstyrelserna att medverka till den nationella politikens genomslag i länen. Länsstyrelsens, och därmed statens roll går från utveckling till bevarande. 
Länsstyrelsen i Gävleborgs län 
framför att den nu föreslagna rollfördelningen skulle bidra till en ökad polarisering mellan staten regionalt, som då har ett uttalat tillsyns- och bevarandeansvar och den föreslagna regionkommunen, med hela ansvaret för tillväxtaspekten. 
Länsstyrelsen i Örebro län 
är positiv till förslaget under förutsättning att regeringen anser att länsstyrelsen fortsättningsvis har en viktig roll i den regionala tillväxtpolitiken. 
Trafikverket, Tillväxtverket, Riksarkivet 
och

Statens Kulturråd 
stödjer promemorians förslag. 
Trafikverket 
ser det som en fördel att regionala kollektivtrafikmyndigheter och ansvaret för upprättande av länstransportplaner finns inom samma regionala organisation. 
Tillväxtverket 
uppmärksammar den utmaning det innebär att det
Prop. 2013/14:46
9
Prop. 2013/14:46
10
regionala utvecklingsansvaret är olika fördelat mellan länen. 
Uppsala läns landsting, Västmanlands läns landsting 
och 
Örebro läns landsting 
tillstyrker förslaget. 
Uppsala läns landsting 
anser att infrastrukturfrågorna med fördel kan hanteras av det organ som är kollektivtrafikmyndighet i länet. 
Regionförbundet i Örebro län 
och 
Region Gävleborg 
tillstyrker förslaget. 
Regionförbundet i Örebro län m.fl. 
förutsätter att finansieringsprincipen ska gälla för de uppgifter som i samband med lagförändringen överförs från statlig till regional nivå.
Örebro, Askersunds, Degerfors, Hallsbergs, Karlskoga m.fl. kommuner 
i Örebro län tillstyrker förslaget. 
Hudiksvalls kommun 
och 
Sandvikens kommun 
tillstyrker förslaget om regionalt utvecklingsansvar i Gävleborgs län. 
Sveriges kommuner och landsting (SKL) 
delar promemorians bedömning och tillstyrker förslaget.

Skälen för regeringens bedömning och förslag: 
I propositionen Regionalt utvecklingsansvar i vissa län (prop. 2009/10:156) gör regeringen bedömningen att en ändrad ansvarsfördelning kan ske i de län som kan uppvisa ett lokalt och regionalt initiativ att ta över vissa statliga uppgifter samt att det inte finns några oklarheter kring den framtida landstingsindelningen. De län som avsågs var Skåne och Västra Götalands län. Förutsättningarna för en ändrad ansvarsfördelning bedömdes också finnas i Hallands och Gotlands län.

Flera länsstyrelser, däribland 
Länsstyrelsen i Värmlands län
, betonar vikten av att en samlad bedömning görs av ansvarsfördelningen mellan staten och den landstingskommunala nivån innan det regionala utvecklingsansvaret överförs till Örebro läns landsting och Gävleborgs läns landsting. Någon liknande invändning lyfts inte fram mot förslaget avseende Jönköpings län.
Promemoriornas förslag innebär att det regionala utvecklingsansvaret förs från samverkansorganet samt vissa uppgifter från länsstyrelsen till landstinget. I dag utför tre landsting och en kommun de uppgifter som promemoriorna föreslår ska tillföras ytterligare tre landsting. I tretton län utförs samma uppgifter, förutom beslut om företagsstöd, av samverkansorgan. Länsstyrelsernas invändningar bör således ses utifrån att utvecklingsansvaret på regional nivå redan åligger politiskt styrda organisationer i 17 av landets 21 län.

Länsstyrelsen i Dalarnas län 
för fram att länsstyrelsens och därmed statens roll riskerar att gå från utveckling till bevarande i och med promemoriornas förslag. Regeringen instämmer i flera länsstyrelsers synpunkter att länsstyrelsen har en viktig roll och besitter en stark tvärsektoriell kompetens som ska användas för att bidra till en hållbar tillväxt och utveckling i länen. I Skåne och Västra Götalands län har länsstyrelsen sedan slutet av 
1990-talet
 haft den roll som blir aktuell för länsstyrelsen i de tre berörda länen. Det aktuella lagstiftningsärendet innebär att landstingen i de tre länen bör tillföras det regionala utvecklingsansvaret och inte länsstyrelsens fortsatta uppdrag i övrigt. Utveckling av länsstyrelsens roll i exempelvis det regionala tillväxtarbetet hanteras inte i detta sammanhang.
Mot bakgrund av att de tre berörda landstingen kan uppvisa ett sådant initiativ, att det inte finns några oklarheter kring den framtida landstingsindelningen samt med hänsyn till den fleråriga erfarenheten av landsting med regionalt utvecklingsansvar bedömer regeringen, i likhet
med flera remissinstanser, att lagen (2010:630) om regionalt utveck- Prop. 2013/14:46 lingsansvar i vissa län ska omfatta även Jönköpings, Örebro och
Gävleborgs län.
Länsstyrelsen i Kronobergs län m.fl. länsstyrelser 
lyfter fram kopplingen mellan företagsstöd och genomförandet av Landsbygdsprogrammet. Länsstyrelserna framhåller fördelarna med att dessa landsbygdsrelaterade uppdrag kvarstår hos länsstyrelsen. Enligt propositionen Regionalt utvecklingsansvar i vissa län är uppgiften att besluta om vissa statliga medel för regionalt tillväxtarbete till vissa delar sammankopplad med att utarbeta och fastställa en strategi för länets utveckling samt med arbetet att samordna insatser för genomförandet av strategin. I propositionen framgår att uppgiften avser att hantera statliga medel och inte att bedriva en egen stödgivning gentemot enskilda företag. Eftersom promemoriorna endast lämnar förslag om vilka län och landsting som bör omfattas av lagen om regionalt utvecklingsansvar i vissa län har inte frågan beretts om eventuell ändring av de uppgifter som enligt lagen berörda landsting ska ha. Vidare är det regeringen som ansvarar för fördelning och styrning av medel inom ramen för regeringens beslut för berörda anslag.

Flera remissinstanser däribland 
Regionförbundet i Jönköpings län 
och 
Regionförbundet i Örebro län 
har lyft fram önskemål om att vallagen bör ändras så att val framöver sker till regionfullmäktige i stället för landstingsfullmäktige. I fråga om val enligt vallagen (2005:837) ansåg regeringen i propositionen Regionalt utvecklingsansvar i vissa län att beteckningen val till landstingsfullmäktige ska användas även vid val i Skåne läns landsting, Hallands läns landsting och Västra Götalands läns landsting. Promemoriornas förslag kring vallagen är endast en redaktionell ändring i berörd paragraf. Frågan om en eventuell ändring av vallagen har inte heller den beretts i detta sammanhang.

6

Konsekvenser

Regeringens bedömning: 
Lagförslagen medför inte några konse-

kvenser utifrån ett socialt, miljömässigt eller jämställdhetsmässigt

perspektiv. Lagförslagen medför däremot konsekvenser avseende

fördelningen av medel inom länen mellan länsstyrelsen och lands-

tinget. Kostnaderna för staten ryms inom befintliga ekonomiska

ramar.

Promemoriornas förslag och bedömningar 
överensstämmer i med

regeringens.

Remissinstanserna: 
Länsstyrelsen i Jönköpings län 
anser att det inte

beskrivs några större konsekvenser av lagändringen. Beslut om före-

tagsstöd och stöd till kommersiell service hanteras av länsstyrelsen.

Beslut om stöd till projektverksamhet hanteras av både länsstyrelsen och

samverkansorganet. Således måste de personella konsekvenserna av en

flytt av samtliga verksamheter beaktas. 
Tillväxtverket, Regionförbundet i

Jönköpings län, Regionförbundet Örebro 
och 
Sveriges kommuner och

landsting

(
SKL) 
noterar detsamma. 
Regionförbundet Örebro
, 
Region

11

Prop. 2013/14:46

Gävleborg
, 
Kumla kommun 
och 
Lekebergs kommun 
anser att det i

regeringens och riksdagens kommande beslut bör synliggöras att en

förändring av lagen om regionalt utvecklingsansvar i vissa län enligt

föreslagen också innebär följdändringar i förordningarna om stöd till

anläggning av kanalisation och kommersiell service.

Skälen för regeringens bedömning: 
Förslagen bedöms inte ge några

konsekvenser utifrån ett socialt, miljömässigt eller jämställdhetsmässigt

perspektiv. Bedömningen görs med hänsyn till att uppgifterna som avses

i lagförslaget inte är nya utan att verksamheten redan bedrivs i länen. Det

som ändras är ansvarsfördelningen mellan berörda aktörer. De personella

konsekvenserna av promemoriornas förslag uppstår i samband med att

verksamhet flyttar över från samverkansorgan och länsstyrelse till

landstingen. I den fortsatta processen ska berörda arbetsgivare beakta

6 b § lagen (1982:80) om anställningsskydd.

Regeringen delar remissinstansernas synpunkter att såväl de

ekonomiska som de personella konsekvenserna av lagförslagen bör lyftas

fram och beaktas. För Jönköpings, Örebro och Gävleborgs län medför

lagförslagen att verksamhet som utförts av respektive läns samverkans-

organ enligt lagen (2002:34) om samverkansorgan i länen går över till

landstinget. Därutöver kan verksamhet kring regionala företagsstöd som

länsstyrelsen utför flyttas över till landstingen. För såväl verksamhet som

utförts av samverkansorganet som verksamhet som bedrivs av läns-

styrelsen kommer landstinget att tilldelas förvaltningsmedel från anslaget

5:1 
Länsstyrelsen m.m. 
inom utgiftsområde 1. I och med att statliga

uppgifter, som regleras i samverkansorganslagen och genom andra

regeringsbeslut, förs till landstingen upphör kommunalförbunden i

Jönköpings, Örebro och Gävleborgs län att utgöra ett samverkansorgan

enligt samverkansorganslagen.

Fördelningen till berörda aktörer av anslag inklusive medel från anslag

1:1 
Regionala tillväxtåtgärder 
inom utgiftsområde 19 sker inom ramen

för regeringens beslut om respektive anslag.

7

Ikraftträdande och

övergångsbestämmelser

Regeringens förslag: 
Lagändringen ska träda i kraft den 1 januari

2015.

Regeringens bedömning: 
Det saknas behov av övergångsbestäm-

melser.

Promemoriornas förslag och bedömningar 
överensstämmer med

regeringens.

Remissinstanserna: 
Regionförbundet i Jönköpings 
län vill särskilt

understryka angelägenheten i att regeringen beslutar om föreslagen

lagändring så fort det är möjligt. Detta lämnar rimligt utrymme för det

förändringsarbete som måste föregå den administrativa organisationens

12

utformning och förberedelse inför den 1 januari 2015.

Skälen för regeringens förslag och bedömning: 
Landstingen i Jön- Prop. 2013/14:46 köpings, Örebro och Gävleborgs län har samtliga ansökt om att få ta över

det regionala utvecklingsansvaret fr.o.m. den 1 januari 2015. Val till landstingsfullmäktige hålls i september 2014. Utgångspunkten för regeringens förslag är landstingens önskemål om att få ta över ansvaret den 1 januari 2015. Några särskilda skäl till varför dessa önskemål inte bör beviljas har inte framkommit och regeringen anser därmed att lagändringen ska träda i kraft den 1 januari 2015. Den föreslagna lagändringen föranleder inte något behov av övergångsbestämmelser.
8 Författningskommentar
Förslaget till lag om ändring i lagen (2010:630) om regionalt utvecklingsansvar i vissa län
1 §
Paragrafen har behandlats i avsnitt 5.
Paragrafen ändras med innebörden att lagens bestämmelser om regionalt tillväxtarbete och länsplaner för regional transportinfrastrukturplanering gäller även Jönköpings, Örebro och Gävleborgs län.
3 §
Paragrafen har behandlats i avsnitt 5.
I paragrafen görs tillägg om att lagen även gäller Jönköpings läns landsting, Örebro läns landsting och Gävleborgs läns landsting.
4 §
Paragrafen har behandlats i avsnitt 5.
I paragrafen görs tillägg om att beteckningen landstingsfullmäktige ska användas vid val även i Jönköpings läns landsting, Örebro läns landsting och Gävleborgs läns landsting.
13
Prop. 2013/14:46
Bilaga 1
14
Sammanfattning av departementspromemorian Regionalt utvecklingsansvar i Jönköpings län (Ds 2012:55)
Enligt lagen (2010:630) om regionalt utvecklingsansvar i vissa län ansvarar landstingen i Skåne, Hallands och Västra Götalands län samt Gotlands kommun för insatser för att skapa en hållbar regional tillväxt och utveckling samt för upprättande och fastställande av länsplaner för regional transportinfrastruktur. Jönköpings läns landsting har den 30 december 2011 lämnat en ansökan till regeringen om att nuvarande Jönköpings län från och med 2015 får bilda en regionkommun med regionalt utvecklingsansvar enligt ovan nämnda lag. I promemorian föreslås ändring av lagen (2010:630) om regionalt utvecklingsansvar i vissa län som medför att lagen även omfattar Jönköpings län och gäller för Jönköpings läns landsting. Promemorians lagförslag innebär att även Jönköpings läns landsting får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Vid val ska dock beteckningen landstingsfullmäktige användas. Lagförslaget föreslås träda i kraft den 1 januari 2015.
Promemorians lagförslag bedöms inte ge några konsekvenser utifrån vare sig ett ekonomiskt, socialt, miljömässigt eller ett jämställdhetsperspektiv. Eventuella kostnader för det allmänna ryms inom befintliga ekonomiska ramar.
Lagförslag i departementspromemorian Regionalt utvecklingsansvar i Jönköpings län (Ds 2012:55)
Härigenom föreskrivs att 1, 3 och 4 §§ lagen (2010:630) om regionalt utvecklingsansvar i vissa län ska ha följande lydelse.
Prop. 2013/14:46
Bilaga 2

Nuvarande lydelse

Föreslagen lydelse

1 §

I denna lag finns bestämmelser

I denna lag finns bestämmelser

om regionalt tillväxtarbete och om

om regionalt tillväxtarbete och om

länsplaner för regional infra-

länsplaner för regional infra-

strukturplanering i Gotlands, Skåne,

strukturplanering

i

Jönköpings,

Hallands och Västra Götalands län.

Gotlands,

Skåne,

Hallands och

Västra Götalands län.

3 §
3

Denna lag gäller för Skåne läns

Denna lag gäller för 
Jönköpings

landsting, Hallands läns landsting

läns landsting, 
Skåne läns lands-

och Västra Götalands läns landsting.

ting, Hallands läns landsting och

Lagen gäller också, med undantag

Västra Götalands

läns landsting.

för 8 § 1, för Gotlands kommun.

Lagen gäller också, med undantag

Det som sägs om ett landsting i 
5-10

för 8 § 1, för Gotlands kommun.

§§ gäller i sådant fall kommunen.

Det som

sägs om

ett

landsting i

5-10
 §§ gäller i sådant fall kom-

munen.

4 §
4

De landsting som avses i denna lag får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om landstingsfullmäktige och landstingsstyrelsen gäller i sådant fall regionfullmäktige och regionstyrelsen.
Gotlands kommun får besluta att kommunfullmäktige och kommunstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om kommunfullmäktige och kommunstyrelsen gäller i sådant fall regionfullmäktige och region-

styrelsen.

Vid genomförande av val enligt

Vid genomförande av val enligt

vallagen

(2005:837)

ska

dock

vallagen

(2005:837)

ska dock

beteckningen landstingsfullmäktige

beteckningen landstingsfullmäktige

användas vid val i Skåne läns

användas vid val i 
Jönköpings läns

landsting,

Hallands läns landsting

landsting,

Skåne läns

landsting,

och Västra Götalands

läns

lands-

Hallands läns landsting och Västra

3

Senaste lydelse 2011:1193.

4

Senaste lydelse 2011:1193.

15
Prop. 2013/14:46 ting. Vid val i Gotlands kommun Bilaga 2 ska beteckningen kommunfull-
mäktige användas.
16
Götalands läns landsting. Vid val i Gotlands kommun ska beteckningen kommunfullmäktige användas.
Förteckning över remissinstanserna avseende departementspromemorian Regionalt utvecklingsansvar i Jönköpings län (Ds 2012:55)
Prop. 2013/14:46
Bilaga 3
Kammarrätten i Göteborg, Länsstyrelsen i Jönköpings län, Länsstyrelsen i Kronobergs län, Länsstyrelsen i Hallands län, Länsstyrelsen i Östergötlands län, Trafikverket, Tillväxtverket, Statens Kulturråd, Post- och Telestyrelsen (PTS), Jönköpings läns landsting, Hallands läns landsting, Kalmar läns landsting, Kronobergs läns landsting, Västra Götalands läns landsting, Regionförbundet i Jönköpings län, Regionförbundet i Södra Småland, Regionförbundet Östsam, Regionförbundet i Kalmar län, Aneby kommun, Gnosjö kommun, Mullsjö kommun, Habo kommun, Gislaveds kommun, Vaggeryds kommun, Jönköpings kommun, Nässjö kommun, Vetlanda kommun, Eksjö kommun, Tranås kommun, Värnamo kommun, Sävsjö kommun Sveriges Kommuner och Landsting (SKL) Tjänstemännens Centralorganisation (TCO), Sveriges Akademikers Centralorganisation (Saco), Landsorganisationen (LO), Företagarna, Småkom och Svenskt Näringsliv. Därutöver har svar kommit in från Örebro läns landsting och Regionförbundet Örebro.
17
Prop. 2013/14:46
Bilaga 4
18
Sammanfattning av departementspromemorian Regionalt utvecklingsansvar i Örebro län och Gävleborgs län (Ds 2013:14)
Enligt lagen (2010:630) om regionalt utvecklingsansvar i vissa län ansvarar landstingen i Skåne, Hallands och Västra Götalands län samt Gotlands kommun för insatser för att skapa en hållbar regional tillväxt och utveckling samt för upprättande och 
fast-ställande
 av länsplaner för regional transportinfrastruktur. Gävleborgs läns landsting har den 14 maj 2012 lämnat en ansökan till regeringen om att i enlighet med lagen om regionalt utvecklingsansvar i vissa län överta det regionala utvecklingsansvaret från och med 2015. Örebro läns landsting har den 10 december 2012 hemställt till regeringen om att få överta det regionala utvecklingsansvaret enligt lagen om regionalt utvecklingsansvar i vissa län från och med 2015. Promemorians lagförslag innebär att även Örebro läns landsting och Gävleborgs läns landsting får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Vid val ska dock beteckningen landstingsfullmäktige användas. Lagförslaget föreslås träda i kraft den 1 januari 2015.
Promemorians lagförslag bedöms inte ge några konsekvenser utifrån vare sig ett ekonomiskt, socialt, miljömässigt eller ett jämställdhetsperspektiv. Eventuella kostnader för det allmänna ryms inom befintliga ekonomiska ramar.
Lagförslag i departementspromemorian Regionalt utvecklingsansvar i Örebro län och Gävleborgs län (Ds 2013:14)
Prop. 2013/14:46
Bilaga 5
Härigenom föreskrivs att 1, 3 och 4 §§ lagen (2010:630) om regionalt utvecklingsansvar i vissa län ska ha följande lydelse.

Nuvarande lydelse

Föreslagen lydelse

1 §

I denna lag finns bestämmelser

I denna lag finns bestämmelser

om regionalt tillväxtarbete och om

om regionalt tillväxtarbete och om

länsplaner för regional infra-

länsplaner för regional infra-

strukturplanering i Gotlands, Skåne,

strukturplanering i Gotlands, Skåne,

Hallands 
och 
Västra Götalands län.

Hallands, Västra Götalands, 
Örebro

och Gävleborgs 
län
.

3 §
5

Denna lag gäller för Skåne läns

Denna lag gäller för Skåne läns

landsting, Hallands läns landsting

landsting, Hallands

läns

landsting,

och Västra Götalands läns lands-

Västra Götalands

läns

landsting,

ting. Lagen gäller också, med

Örebro läns landsting och Gävle-

undantag för 8 § 1, för Gotlands

borgs läns landsting. 
Lagen gäller

kommun. Det som sägs om ett

också, med undantag för 8 § 1, för

landsting i 
5-10
 §§ gäller i sådant

Gotlands kommun. Det som sägs

fall kommunen.

om ett landsting i 
5-10
 §§ gäller i

sådant fall kommunen.

4 §
6

De landsting som avses i denna lag får besluta att landstingsfullmäktige och landstingsstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om landstingsfullmäktige och landstingsstyrelsen gäller i sådant fall regionfullmäktige och regionstyrelsen.
Gotlands kommun får besluta att kommunfullmäktige och kommunstyrelsen i stället ska betecknas regionfullmäktige och regionstyrelsen. Det som sägs i andra författningar om kommunfullmäktige och kommunstyrelsen gäller i sådant fall regionfullmäktige och regionstyrelsen.
Vid genomförande av val enligt Vid genomförande av val enligt vallagen (2005:837) ska dock vallagen (2005:837) ska dock beteckningen landstingsfullmäktige beteckningen landstingsfullmäktige användas vid val i Skåne läns användas vid val i Skåne läns

5

Senaste lydelse 2011:1193.

6

Senaste lydelse 2011:1193.

19
Prop. 2013/14:46
Bilaga 5
20
landsting, Hallands läns landsting 
och 
Västra Götalands läns landsting. Vid val i Gotlands kommun ska beteckningen kommunfullmäktige användas.
landsting, Hallands läns landsting, Västra Götalands läns landsting,
Örebro läns landsting och Gävleborgs läns landsting. 
Vid val i Gotlands kommun ska beteckningen kommunfullmäktige användas.

Förteckning över remissinstanserna avseende departementspromemorian Regionalt utvecklingsansvar i Örebro län och Gävleborgs län (Ds 2013:14)
Prop. 2013/14:46
Bilaga 6
Kammarrätten i Göteborg, Länsstyrelsen i Uppsala län, Länsstyrelsen i Västmanlands län, Länsstyrelsen i Värmlands län, Länsstyrelsen i Dalarnas län, Länsstyrelsen i Örebro län, Länsstyrelsen i Gävleborgs län, Länsstyrelsen i Västernorrlands län, Trafikverket, Tillväxtverket, Post- och Telestyrelsen (PTS), Statens Kulturråd, Riksarkivet, Riksantikvarieämbetet, Stiftelsen Svenska Filminstitutet, Uppsala läns landsting, Västmanlands läns landsting, Örebro läns landsting, Dalarnas läns landsting, Gävleborgs läns landsting, Region Värmland, Regionförbundet Gävleborg, Regionförbundet i Örebro län, Askersunds kommun, Degerfors kommun, Hallsbergs kommun, Hällefors kommun, Karlskoga kommun, Kumla kommun, Laxå kommun, Lekebergs kommun, Lindesbergs kommun, Ljusnarsbergs kommun, Nora kommun, Örebro kommun, Hofors kommun, Bollnäs kommun, Gävle kommun, Ljusdals kommun, Nordanstigs kommun, Ockelbo kommun, Ovanåkers kommun, Söderhamns kommun, Hudiksvalls kommun, Sandvikens kommun, Tjänstemännens Centralorganisation (TCO), Sveriges Akademikers Centralorganisation (Saco), Landsorganisationen (LO), Företagarna, Småkom och Svenskt Näringsliv. Därutöver har svar kommit in från Mellansvenska Handelskammaren.
21

Prop. 2013/14:46

Socialdepartementet

Utdrag ur protokoll vid regeringssammanträde den 21 november 2013

Närvarande: Statsministern Reinfeldt, ordförande, och statsråden

Björklund, Ask, Erlandsson, Hägglund, Borg, Billström,

Adelsohn Liljeroth, Ohlsson, Norman, Attefall, Engström, Kristersson,

Elmsäter-Svärd,
 Ullenhag, Hatt, Lööf, Enström, Arnholm, Svantesson

Föredragande: statsrådet Attefall

Regeringen beslutar proposition 2013/14:46 Regionalt utvecklingsansvar

i Jönköpings, Örebro och Gävleborgs län.
22