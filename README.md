**Det finns egentligen två separata spår här**

** 1. Läsa text.ipynb **
Denna importerar två textfiler med konsekvensanalyser och splittar upp allt till en Pands DataFrame samt lägger till meta-data

** 2. Identify.ipnb ** (OBS GAMMAL)
Detta skript är embryot till skriptet som ska kunna identifiera ett kapitel i en text baserat på regex-regler och innehållsförteckningen.

** 2.1 Identify_loop.py **
Detta är den huvudsakliga produkten
Skript som söker rätt på ett konsekvensanalysavsnittet och sätter in det i en Pandas DataFrame

